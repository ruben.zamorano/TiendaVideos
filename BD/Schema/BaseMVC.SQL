USE TiendaVideos
;

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FK_Comentarios_Usuarios_Base_Usuarios') AND OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE Comentarios_Usuarios DROP CONSTRAINT FK_Comentarios_Usuarios_Base_Usuarios
;

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FK_Comentarios_Perfiles_Base_Perfiles') AND OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE Comentarios_Perfiles DROP CONSTRAINT FK_Comentarios_Perfiles_Base_Perfiles
;

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FK_Comentarios_Areas_Base_Areas') AND OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE Comentarios_Areas DROP CONSTRAINT FK_Comentarios_Areas_Base_Areas
;

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FK_Base_Restriciones_X_Area_Base_Modulos') AND OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE Base_Restriciones_X_Area DROP CONSTRAINT FK_Base_Restriciones_X_Area_Base_Modulos
;

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FK_Base_Derechos_X_Perfil_Base_Areas') AND OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE Base_Derechos_X_Perfil DROP CONSTRAINT FK_Base_Derechos_X_Perfil_Base_Areas
;

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FK_Base_Derechos_X_Perfil_Base_Perfiles') AND OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE Base_Derechos_X_Perfil DROP CONSTRAINT FK_Base_Derechos_X_Perfil_Base_Perfiles
;

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FK_Base_Usuario_X_Perfil_Base_Perfiles') AND OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE Base_Usuario_X_Perfil DROP CONSTRAINT FK_Base_Usuario_X_Perfil_Base_Perfiles
;

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FK_Base_Usuario_X_Perfil_Base_Usuarios') AND OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE Base_Usuario_X_Perfil DROP CONSTRAINT FK_Base_Usuario_X_Perfil_Base_Usuarios
;

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FK_Base_Modulos_Base_Areas') AND OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE Base_Modulos DROP CONSTRAINT FK_Base_Modulos_Base_Areas
;


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Comentarios_Usuarios') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Comentarios_Usuarios
;

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Comentarios_Perfiles') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Comentarios_Perfiles
;

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Comentarios_Areas') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Comentarios_Areas
;

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Base_Restriciones_X_Area') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Base_Restriciones_X_Area
;

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Base_Derechos_X_Perfil') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Base_Derechos_X_Perfil
;

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Base_Usuario_X_Perfil') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Base_Usuario_X_Perfil
;

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Base_Modulos') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Base_Modulos
;

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Base_Perfiles') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Base_Perfiles
;

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Base_Areas') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Base_Areas
;

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Base_Usuarios') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Base_Usuarios
;


CREATE TABLE Comentarios_Usuarios ( 
	nIdComentario int identity(1,1)  NOT NULL,
	cLogin varchar(50) NOT NULL,
	cComentario varchar(50) NOT NULL,
	dFechaRegistro datetime,
	cUsuarioRegistro varchar(50) NOT NULL,
	cMaquinaRegistro varchar(50) NOT NULL,
	dFechaUltimaModificacion datetime NOT NULL,
	cUsuarioUltimaModificacion varchar(50),
	cMaquinaUltimaModificacion varchar(50)
)
;

CREATE TABLE Comentarios_Perfiles ( 
	nIdComentario int identity(1,1)  NOT NULL,
	nIdPerfil int NOT NULL,
	cComentario varchar(50) NOT NULL,
	dFechaRegistro datetime,
	cUsuarioRegistro varchar(50) NOT NULL,
	cMaquinaRegistro varchar(50) NOT NULL,
	dFechaUltimaModificacion datetime NOT NULL,
	cUsuarioUltimaModificacion varchar(50),
	cMaquinaUltimaModificacion varchar(50)
)
;

CREATE TABLE Comentarios_Areas ( 
	nIdComentario int identity(1,1)  NOT NULL,
	nIdArea int NOT NULL,
	cComentario varchar(50) NOT NULL,
	dFechaRegistro datetime,
	cUsuarioRegistro varchar(50) NOT NULL,
	cMaquinaRegistro varchar(50) NOT NULL,
	dFechaUltimaModificacion datetime NOT NULL,
	cUsuarioUltimaModificacion varchar(50),
	cMaquinaUltimaModificacion varchar(50)
)
;

CREATE TABLE Base_Restriciones_X_Area ( 
	nRestriccion int identity(1,1)  NOT NULL,
	cLogin varchar(50) NOT NULL,
	nModulo int NOT NULL
)
;

CREATE TABLE Base_Derechos_X_Perfil ( 
	nPerfil int NOT NULL,
	nArea int NOT NULL,
	nIdDerecho int identity(1,1)  NOT NULL
)
;

CREATE TABLE Base_Usuario_X_Perfil ( 
	nPerfilUsuario int identity(1,1)  NOT NULL,
	nPerfil int NOT NULL,
	cLogin varchar(50) NOT NULL
)
;

CREATE TABLE Base_Modulos ( 
	nModulo int identity(1,1)  NOT NULL,
	cDescripcion varchar(20) NOT NULL,
	nArea int NOT NULL,
	cUrlModulo varchar(50),
	cIcono varchar(20)
)
;

CREATE TABLE Base_Perfiles ( 
	nPerfil int identity(1,1)  NOT NULL,
	cDescripcion varchar(20) NOT NULL
)
;

CREATE TABLE Base_Areas ( 
	nArea int identity(1,1)  NOT NULL,
	cDescripcion varchar(20) NOT NULL,
	cIcono varchar(20)
)
;

CREATE TABLE Base_Usuarios ( 
	cLogin varchar(50) NOT NULL,
	nIdUsuario int identity(1,1)  NOT NULL,
	cPassword varchar(50) NOT NULL,
	cDescripcion varchar(50),
	dFechaRegistro datetime
)
;


ALTER TABLE Comentarios_Usuarios ADD CONSTRAINT PK_Comentarios_Usuarios 
	PRIMARY KEY CLUSTERED (nIdComentario)
;

ALTER TABLE Comentarios_Perfiles ADD CONSTRAINT PK_Comentarios_Perfiles 
	PRIMARY KEY CLUSTERED (nIdComentario)
;

ALTER TABLE Comentarios_Areas ADD CONSTRAINT PK_Comentarios_Areas 
	PRIMARY KEY CLUSTERED (nIdComentario)
;

ALTER TABLE Base_Restriciones_X_Area ADD CONSTRAINT PK_Base_Restriciones_X_Area 
	PRIMARY KEY CLUSTERED (nRestriccion)
;

ALTER TABLE Base_Derechos_X_Perfil ADD CONSTRAINT PK_Base_Derechos_X_Perfil 
	PRIMARY KEY CLUSTERED (nIdDerecho)
;

ALTER TABLE Base_Usuario_X_Perfil ADD CONSTRAINT PK_Base_Usuario_X_Perfil 
	PRIMARY KEY CLUSTERED (nPerfilUsuario)
;

ALTER TABLE Base_Modulos ADD CONSTRAINT PK_Base_Modulos 
	PRIMARY KEY CLUSTERED (nModulo)
;

ALTER TABLE Base_Perfiles ADD CONSTRAINT PK_Base_Perfiles 
	PRIMARY KEY CLUSTERED (nPerfil)
;

ALTER TABLE Base_Areas ADD CONSTRAINT PK_Base_Areas 
	PRIMARY KEY CLUSTERED (nArea)
;

ALTER TABLE Base_Usuarios ADD CONSTRAINT PK_Base_Usuarios 
	PRIMARY KEY CLUSTERED (cLogin)
;


ALTER TABLE Base_Usuarios
	ADD CONSTRAINT UQ_Base_Usuarios_nIdUsuario UNIQUE (nIdUsuario)
;


ALTER TABLE Comentarios_Usuarios ADD CONSTRAINT FK_Comentarios_Usuarios_Base_Usuarios 
	FOREIGN KEY (cLogin) REFERENCES Base_Usuarios (cLogin)
;

ALTER TABLE Comentarios_Perfiles ADD CONSTRAINT FK_Comentarios_Perfiles_Base_Perfiles 
	FOREIGN KEY (nIdPerfil) REFERENCES Base_Perfiles (nPerfil)
;

ALTER TABLE Comentarios_Areas ADD CONSTRAINT FK_Comentarios_Areas_Base_Areas 
	FOREIGN KEY (nIdArea) REFERENCES Base_Areas (nArea)
;

ALTER TABLE Base_Restriciones_X_Area ADD CONSTRAINT FK_Base_Restriciones_X_Area_Base_Modulos 
	FOREIGN KEY (nModulo) REFERENCES Base_Modulos (nModulo)
;

ALTER TABLE Base_Derechos_X_Perfil ADD CONSTRAINT FK_Base_Derechos_X_Perfil_Base_Areas 
	FOREIGN KEY (nArea) REFERENCES Base_Areas (nArea)
;

ALTER TABLE Base_Derechos_X_Perfil ADD CONSTRAINT FK_Base_Derechos_X_Perfil_Base_Perfiles 
	FOREIGN KEY (nPerfil) REFERENCES Base_Perfiles (nPerfil)
;

ALTER TABLE Base_Usuario_X_Perfil ADD CONSTRAINT FK_Base_Usuario_X_Perfil_Base_Perfiles 
	FOREIGN KEY (nPerfil) REFERENCES Base_Perfiles (nPerfil)
;

ALTER TABLE Base_Usuario_X_Perfil ADD CONSTRAINT FK_Base_Usuario_X_Perfil_Base_Usuarios 
	FOREIGN KEY (cLogin) REFERENCES Base_Usuarios (cLogin)
;

ALTER TABLE Base_Modulos ADD CONSTRAINT FK_Base_Modulos_Base_Areas 
	FOREIGN KEY (nArea) REFERENCES Base_Areas (nArea)
;
