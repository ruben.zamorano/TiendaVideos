USE [BaseMVC]
GO
/****** Object:  Table [dbo].[Base_Sucursales]    Script Date: 08/19/2016 23:07:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Base_Sucursales](
	[cDescripcion] [varchar](50) NULL,
	[nSucursal] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Base_Sucursales] PRIMARY KEY CLUSTERED 
(
	[nSucursal] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Base_Sucursales] ON
INSERT [dbo].[Base_Sucursales] ([cDescripcion], [nSucursal]) VALUES (N'San Juan del Rio', 1)
INSERT [dbo].[Base_Sucursales] ([cDescripcion], [nSucursal]) VALUES (N'Culiacan', 2)
SET IDENTITY_INSERT [dbo].[Base_Sucursales] OFF
/****** Object:  Table [dbo].[Base_Emisores]    Script Date: 08/19/2016 23:07:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Base_Emisores](
	[nEmisor] [int] IDENTITY(1,1) NOT NULL,
	[cDescripcion] [varchar](50) NULL,
 CONSTRAINT [PK_Base_Emisores] PRIMARY KEY CLUSTERED 
(
	[nEmisor] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Base_Emisores] ON
INSERT [dbo].[Base_Emisores] ([nEmisor], [cDescripcion]) VALUES (1, N'uno')
SET IDENTITY_INSERT [dbo].[Base_Emisores] OFF
/****** Object:  Table [dbo].[Base_Usuarios]    Script Date: 08/19/2016 23:07:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Base_Usuarios](
	[cLogin] [varchar](50) NOT NULL,
	[nIdUsuario] [int] IDENTITY(1,1) NOT NULL,
	[nSucursal] [int] NULL,
	[cPassword] [varchar](50) NOT NULL,
	[cDescripcion] [varchar](50) NULL,
	[dFechaRegistro] [datetime] NULL,
 CONSTRAINT [PK_Base_Usuarios] PRIMARY KEY CLUSTERED 
(
	[cLogin] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_Base_Usuarios_nIdUsuario] UNIQUE NONCLUSTERED 
(
	[nIdUsuario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Base_Usuarios] ON
INSERT [dbo].[Base_Usuarios] ([cLogin], [nIdUsuario], [nSucursal], [cPassword], [cDescripcion], [dFechaRegistro]) VALUES (N'ruben', 1, NULL, N'GpKunP6VXwV8xw3qOHusFQ==', N'ruben', CAST(0x0000A58000000000 AS DateTime))
SET IDENTITY_INSERT [dbo].[Base_Usuarios] OFF
/****** Object:  Table [dbo].[Base_Areas]    Script Date: 08/19/2016 23:07:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Base_Areas](
	[nArea] [int] IDENTITY(1,1) NOT NULL,
	[cDescripcion] [varchar](20) NOT NULL,
	[cIcono] [varchar](20) NULL,
 CONSTRAINT [PK_Base_Areas] PRIMARY KEY CLUSTERED 
(
	[nArea] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Base_Areas] ON
INSERT [dbo].[Base_Areas] ([nArea], [cDescripcion], [cIcono]) VALUES (10, N'Admin', N'dashboard')
SET IDENTITY_INSERT [dbo].[Base_Areas] OFF
/****** Object:  Table [dbo].[Base_Perfiles]    Script Date: 08/19/2016 23:07:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Base_Perfiles](
	[nPerfil] [int] IDENTITY(1,1) NOT NULL,
	[cDescripcion] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Base_Perfiles] PRIMARY KEY CLUSTERED 
(
	[nPerfil] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Base_Perfiles] ON
INSERT [dbo].[Base_Perfiles] ([nPerfil], [cDescripcion]) VALUES (6, N'SuperAdmin')
SET IDENTITY_INSERT [dbo].[Base_Perfiles] OFF
/****** Object:  Table [dbo].[Comentarios_Usuarios]    Script Date: 08/19/2016 23:07:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Comentarios_Usuarios](
	[nIdComentario] [int] IDENTITY(1,1) NOT NULL,
	[cLogin] [varchar](50) NOT NULL,
	[cComentario] [varchar](50) NOT NULL,
	[dFechaRegistro] [datetime] NULL,
	[cUsuarioRegistro] [varchar](50) NOT NULL,
	[cMaquinaRegistro] [varchar](50) NOT NULL,
	[dFechaUltimaModificacion] [datetime] NULL,
	[cUsuarioUltimaModificacion] [varchar](50) NULL,
	[cMaquinaUltimaModificacion] [varchar](50) NULL,
 CONSTRAINT [PK_Comentarios_Usuarios] PRIMARY KEY CLUSTERED 
(
	[nIdComentario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Comentarios_Perfiles]    Script Date: 08/19/2016 23:07:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Comentarios_Perfiles](
	[nIdComentario] [int] IDENTITY(1,1) NOT NULL,
	[nIdPerfil] [int] NOT NULL,
	[cComentario] [varchar](50) NOT NULL,
	[dFechaRegistro] [datetime] NULL,
	[cUsuarioRegistro] [varchar](50) NOT NULL,
	[cMaquinaRegistro] [varchar](50) NOT NULL,
	[dFechaUltimaModificacion] [datetime] NULL,
	[cUsuarioUltimaModificacion] [varchar](50) NULL,
	[cMaquinaUltimaModificacion] [varchar](50) NULL,
 CONSTRAINT [PK_Comentarios_Perfiles] PRIMARY KEY CLUSTERED 
(
	[nIdComentario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Comentarios_Areas]    Script Date: 08/19/2016 23:07:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Comentarios_Areas](
	[nIdComentario] [int] IDENTITY(1,1) NOT NULL,
	[nIdArea] [int] NOT NULL,
	[cComentario] [varchar](50) NOT NULL,
	[dFechaRegistro] [datetime] NULL,
	[cUsuarioRegistro] [varchar](50) NOT NULL,
	[cMaquinaRegistro] [varchar](50) NOT NULL,
	[dFechaUltimaModificacion] [datetime] NULL,
	[cUsuarioUltimaModificacion] [varchar](50) NULL,
	[cMaquinaUltimaModificacion] [varchar](50) NULL,
 CONSTRAINT [PK_Comentarios_Areas] PRIMARY KEY CLUSTERED 
(
	[nIdComentario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Base_Modulos]    Script Date: 08/19/2016 23:07:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Base_Modulos](
	[nModulo] [int] IDENTITY(1,1) NOT NULL,
	[cDescripcion] [varchar](20) NOT NULL,
	[nArea] [int] NOT NULL,
	[cUrlModulo] [varchar](50) NULL,
	[cIcono] [varchar](20) NULL,
 CONSTRAINT [PK_Base_Modulos] PRIMARY KEY CLUSTERED 
(
	[nModulo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Base_Modulos] ON
INSERT [dbo].[Base_Modulos] ([nModulo], [cDescripcion], [nArea], [cUrlModulo], [cIcono]) VALUES (37, N'Configuracion', 10, N'/Admin/Configuracion', NULL)
SET IDENTITY_INSERT [dbo].[Base_Modulos] OFF
/****** Object:  Table [dbo].[Base_Usuario_X_Perfil]    Script Date: 08/19/2016 23:07:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Base_Usuario_X_Perfil](
	[nPerfilUsuario] [int] IDENTITY(1,1) NOT NULL,
	[nPerfil] [int] NOT NULL,
	[cLogin] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Base_Usuario_X_Perfil] PRIMARY KEY CLUSTERED 
(
	[nPerfilUsuario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Base_Usuario_X_Perfil] ON
INSERT [dbo].[Base_Usuario_X_Perfil] ([nPerfilUsuario], [nPerfil], [cLogin]) VALUES (7, 6, N'ruben')
SET IDENTITY_INSERT [dbo].[Base_Usuario_X_Perfil] OFF
/****** Object:  Table [dbo].[Base_Derechos_X_Perfil]    Script Date: 08/19/2016 23:07:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Base_Derechos_X_Perfil](
	[nPerfil] [int] NOT NULL,
	[nArea] [int] NOT NULL,
	[nIdDerecho] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Base_Derechos_X_Perfil] PRIMARY KEY CLUSTERED 
(
	[nIdDerecho] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Base_Derechos_X_Perfil] ON
INSERT [dbo].[Base_Derechos_X_Perfil] ([nPerfil], [nArea], [nIdDerecho]) VALUES (6, 10, 35)
SET IDENTITY_INSERT [dbo].[Base_Derechos_X_Perfil] OFF
/****** Object:  Table [dbo].[Base_Restriciones_X_Area]    Script Date: 08/19/2016 23:07:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Base_Restriciones_X_Area](
	[nRestriccion] [int] IDENTITY(1,1) NOT NULL,
	[cLogin] [varchar](50) NOT NULL,
	[nModulo] [int] NOT NULL,
 CONSTRAINT [PK_Base_Restriciones_X_Area] PRIMARY KEY CLUSTERED 
(
	[nRestriccion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  ForeignKey [FK_Base_Derechos_X_Perfil_Base_Areas]    Script Date: 08/19/2016 23:07:32 ******/
ALTER TABLE [dbo].[Base_Derechos_X_Perfil]  WITH CHECK ADD  CONSTRAINT [FK_Base_Derechos_X_Perfil_Base_Areas] FOREIGN KEY([nArea])
REFERENCES [dbo].[Base_Areas] ([nArea])
GO
ALTER TABLE [dbo].[Base_Derechos_X_Perfil] CHECK CONSTRAINT [FK_Base_Derechos_X_Perfil_Base_Areas]
GO
/****** Object:  ForeignKey [FK_Base_Derechos_X_Perfil_Base_Perfiles]    Script Date: 08/19/2016 23:07:32 ******/
ALTER TABLE [dbo].[Base_Derechos_X_Perfil]  WITH CHECK ADD  CONSTRAINT [FK_Base_Derechos_X_Perfil_Base_Perfiles] FOREIGN KEY([nPerfil])
REFERENCES [dbo].[Base_Perfiles] ([nPerfil])
GO
ALTER TABLE [dbo].[Base_Derechos_X_Perfil] CHECK CONSTRAINT [FK_Base_Derechos_X_Perfil_Base_Perfiles]
GO
/****** Object:  ForeignKey [FK_Base_Modulos_Base_Areas]    Script Date: 08/19/2016 23:07:32 ******/
ALTER TABLE [dbo].[Base_Modulos]  WITH CHECK ADD  CONSTRAINT [FK_Base_Modulos_Base_Areas] FOREIGN KEY([nArea])
REFERENCES [dbo].[Base_Areas] ([nArea])
GO
ALTER TABLE [dbo].[Base_Modulos] CHECK CONSTRAINT [FK_Base_Modulos_Base_Areas]
GO
/****** Object:  ForeignKey [FK_Base_Restriciones_X_Area_Base_Modulos]    Script Date: 08/19/2016 23:07:32 ******/
ALTER TABLE [dbo].[Base_Restriciones_X_Area]  WITH CHECK ADD  CONSTRAINT [FK_Base_Restriciones_X_Area_Base_Modulos] FOREIGN KEY([nModulo])
REFERENCES [dbo].[Base_Modulos] ([nModulo])
GO
ALTER TABLE [dbo].[Base_Restriciones_X_Area] CHECK CONSTRAINT [FK_Base_Restriciones_X_Area_Base_Modulos]
GO
/****** Object:  ForeignKey [FK_Base_Usuario_X_Perfil_Base_Perfiles]    Script Date: 08/19/2016 23:07:32 ******/
ALTER TABLE [dbo].[Base_Usuario_X_Perfil]  WITH CHECK ADD  CONSTRAINT [FK_Base_Usuario_X_Perfil_Base_Perfiles] FOREIGN KEY([nPerfil])
REFERENCES [dbo].[Base_Perfiles] ([nPerfil])
GO
ALTER TABLE [dbo].[Base_Usuario_X_Perfil] CHECK CONSTRAINT [FK_Base_Usuario_X_Perfil_Base_Perfiles]
GO
/****** Object:  ForeignKey [FK_Base_Usuario_X_Perfil_Base_Usuarios]    Script Date: 08/19/2016 23:07:32 ******/
ALTER TABLE [dbo].[Base_Usuario_X_Perfil]  WITH CHECK ADD  CONSTRAINT [FK_Base_Usuario_X_Perfil_Base_Usuarios] FOREIGN KEY([cLogin])
REFERENCES [dbo].[Base_Usuarios] ([cLogin])
GO
ALTER TABLE [dbo].[Base_Usuario_X_Perfil] CHECK CONSTRAINT [FK_Base_Usuario_X_Perfil_Base_Usuarios]
GO
/****** Object:  ForeignKey [FK_Comentarios_Areas_Base_Areas]    Script Date: 08/19/2016 23:07:32 ******/
ALTER TABLE [dbo].[Comentarios_Areas]  WITH CHECK ADD  CONSTRAINT [FK_Comentarios_Areas_Base_Areas] FOREIGN KEY([nIdArea])
REFERENCES [dbo].[Base_Areas] ([nArea])
GO
ALTER TABLE [dbo].[Comentarios_Areas] CHECK CONSTRAINT [FK_Comentarios_Areas_Base_Areas]
GO
/****** Object:  ForeignKey [FK_Comentarios_Perfiles_Base_Perfiles]    Script Date: 08/19/2016 23:07:32 ******/
ALTER TABLE [dbo].[Comentarios_Perfiles]  WITH CHECK ADD  CONSTRAINT [FK_Comentarios_Perfiles_Base_Perfiles] FOREIGN KEY([nIdPerfil])
REFERENCES [dbo].[Base_Perfiles] ([nPerfil])
GO
ALTER TABLE [dbo].[Comentarios_Perfiles] CHECK CONSTRAINT [FK_Comentarios_Perfiles_Base_Perfiles]
GO
/****** Object:  ForeignKey [FK_Comentarios_Usuarios_Base_Usuarios]    Script Date: 08/19/2016 23:07:32 ******/
ALTER TABLE [dbo].[Comentarios_Usuarios]  WITH CHECK ADD  CONSTRAINT [FK_Comentarios_Usuarios_Base_Usuarios] FOREIGN KEY([cLogin])
REFERENCES [dbo].[Base_Usuarios] ([cLogin])
GO
ALTER TABLE [dbo].[Comentarios_Usuarios] CHECK CONSTRAINT [FK_Comentarios_Usuarios_Base_Usuarios]
GO
