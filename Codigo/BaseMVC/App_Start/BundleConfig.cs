﻿using System.Web;
using System.Web.Optimization;

namespace BaseMVC
{
    public class BundleConfig
    {
        // Para obtener más información acerca de Bundling, consulte http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.UseCdn = true;
            RegisterStyleBundles(bundles);
            RegisterJavascriptBundles(bundles);

            BundleTable.EnableOptimizations = true;
        }

        private static void RegisterStyleBundles(BundleCollection bundles)
        {

            //General
            bundles.Add(new StyleBundle("~/Content/General_CSS").Include(
                "~/Content/General/IconFix.css"
                ));

           
            //AdminLTE
            bundles.Add(new StyleBundle("~/Content/AdminLTE_CSS").Include(
                 "~/Content/AdminLTE/dist/css/AdminLTE.min.css",
                "~/Content/AdminLTE/dist/css/skins/_all-skins.min.css",
                  "~/Content/AdminLTE/bootstrap/css/bootstrap.min.css"
                ));



            //Angular Material
            bundles.Add(new StyleBundle("~/Content/Material_CSS").Include(
                 "~/Content/General/font-awesome.css",
                 "~/Content/Angular_Modules/angular-material/angular-material.min.css",
                 "~/Content/Angular_Modules/md-data-table/md-data-table.css",
                 "~/Content/Angular_Modules/md-data-table/app.css"
                ));



            //CDN
            //bundles.Add(new StyleBundle("~/Content/MaterialIcons", "https://fonts.googleapis.com/icon?family=Material+Icons").Include());
            //bundles.Add(new StyleBundle("~/Content/Ionicons", "https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css").Include());



            //bundles.Add(new StyleBundle("~/Content/error123").Include(
            //        "~/Content/Error/css/style.css"
            //    ));


          




        }

        private static void RegisterJavascriptBundles(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jQuery/jquery-2.1.4.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.unobtrusive*",
            //            "~/Scripts/jquery.validate*"));

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información sobre los formularios. De este modo, estará
            // preparado para la producción y podrá utilizar la herramienta de creación disponible en http://modernizr.com para seleccionar solo las pruebas que necesite.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/jQuery/modernizr-2.5.3"));


            //Angular Material
            bundles.Add(new ScriptBundle("~/bundles/Angular").Include(
                        "~/Content/Angular_Modules/angular/angular.min.js",
                        "~/Content/Angular_Modules/angular-aria/angular-aria.min.js",
                        "~/Content/Angular_Modules/angular-animate/angular-animate.min.js",
                        "~/Content/Angular_Modules/angular-material/angular-material.min.js",
                        "~/Content/Angular_Modules/md-data-table/md-data-table.js",
                        "~/Scripts/General/app.js",
                        "~/Scripts/General/ctrl.js"
                        ));
            //bundles.Add(new ScriptBundle("~/bundles/bootstrapJS").Include(
            //       "~/Scripts/jQuery/bootstrap.min.js"
            //       ));

            const string ANGULAR_APP_ROOT = "~/Scripts/General/";
            const string VIRTUAL_BUNDLE_PATH = ANGULAR_APP_ROOT + "main.js";

            var scriptBundle = new ScriptBundle(VIRTUAL_BUNDLE_PATH)
                .IncludeDirectory(ANGULAR_APP_ROOT, "*.js", searchSubdirectories: true);
           
            var scriptBundle2 = new ScriptBundle(VIRTUAL_BUNDLE_PATH)
                .Include(ANGULAR_APP_ROOT + "app.js",
                ANGULAR_APP_ROOT + "ctrl.js")
                .IncludeDirectory(ANGULAR_APP_ROOT, "*.js", searchSubdirectories: true);

            bundles.Add(scriptBundle2);


            //AdminLTE
            bundles.Add(new ScriptBundle("~/bundles/AdminLTE").Include(
                  "~/Content/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js",
                  "~/Content/AdminLTE/bootstrap/js/bootstrap.js",
                  "~/Content/AdminLTE/plugins/slimScroll/jquery.slimscroll.js",
                  "~/Content/AdminLTE/plugins/fastclick/fastclick.js",
                  "~/Content/AdminLTE/dist/js/app.js",
                  "~/Content/AdminLTE/dist/js/demo.js"
                  ));

            bundles.Add(new ScriptBundle("~/bundles/LoginCtrl").Include(
                    "~/Scripts/General/app.js",
                    "~/Scripts/General/ctrl.js",
                    "~/Scripts/General/directivas.js"
                    
                       ));




            //Area admin
            bundles.Add(new ScriptBundle("~/bundles/Areas/Admin")
                .Include("~/Scripts/Areas/Admin/app.js")
                .IncludeDirectory("~/Scripts/Areas/Admin/", "*.js", searchSubdirectories: true
                ));

            bundles.Add(new ScriptBundle("~/bundles/Areas/Renta")
               .Include("~/Scripts/Areas/Renta/app.js")
               .IncludeDirectory("~/Scripts/Areas/Renta/", "*.js", searchSubdirectories: true
               ));

            bundles.Add(new ScriptBundle("~/bundles/Areas/Sucursal")
               .Include("~/Scripts/Areas/Sucursal/app.js")
               .IncludeDirectory("~/Scripts/Areas/Sucursal/", "*.js", searchSubdirectories: true
               ));

          
          
            
           

            
           


            

          

            

           





        } 
    }
}
