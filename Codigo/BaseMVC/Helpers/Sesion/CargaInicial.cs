﻿using BaseMVC.Helpers.Sesion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseMVC.Models
{

    public class CargaInicial 
    {
        public DataSesion DataSesion { get; set; }
        public List<DataArea> DataNav { get; set; }
        public string cModuloActual { get; set; }
        public int nIdAreaActual { get; set; }
        static int nLayout { get; set; }
        public enum Areas
        {
            Admin = 1,Area1 =2,
        };
        public CargaInicial()
        {
            DataSesion = (DataSesion)HttpContext.Current.Session["DataSesion"];
            DataNav = (List<DataArea>)HttpContext.Current.Session["DataNav"];
        
        }
    }
}