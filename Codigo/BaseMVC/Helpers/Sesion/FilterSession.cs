﻿using BaseMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BaseMVC.Helpers
{
    class FilterSession : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //var enableSecurity = ConfigurationManager.AppSettings[ViewConstants.ENABLE_SECURITY];

            DataSesion Sesion = (DataSesion)HttpContext.Current.Session["DataSesion"];
            string Controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToUpperInvariant();
            string Action = filterContext.ActionDescriptor.ActionName.ToUpperInvariant();
            if (Sesion.bSesionActiva)
            {

                if (Controller == "ACCOUNT" && Action == "LOGIN")
                {
                    RouteValueDictionary redirectTargetDictionary = new RouteValueDictionary();
                    //redirectTargetDictionary.Add("action", Action);
                    //redirectTargetDictionary.Add("controller",Controller);
                    //filterContext.Result = new RedirectToRouteResult(redirectTargetDictionary);
                    redirectTargetDictionary.Add("action", "Index");
                    redirectTargetDictionary.Add("controller", "Configuracion");
                    redirectTargetDictionary.Add("area", "Admin");
                    filterContext.Result = new RedirectToRouteResult(redirectTargetDictionary);
                }
            }
            else
            {
                if (Controller != "ACCOUNT" && Action != "LOGIN")
                {
                    RouteValueDictionary redirectTargetDictionary = new RouteValueDictionary();
                    redirectTargetDictionary.Add("action", "logIn");
                    redirectTargetDictionary.Add("controller", "Account");
                    redirectTargetDictionary.Add("area","");
                    filterContext.Result = new RedirectToRouteResult(redirectTargetDictionary);
                }
            }
        }
    }
}