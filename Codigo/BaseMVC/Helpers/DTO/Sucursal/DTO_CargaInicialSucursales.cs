using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseMVC.Helpers.DTO.Sucursal
{
    public class DTO_CargaInicialSucursales
    {
        public string cUrl { get; set; }
        public bool bError { get; set; }
        public string cMensaje { get; set; }
        public List<DTO_Sucursal> lstSucursales { get; set; }

        public DTO_CargaInicialSucursales()
        {
            bError = false;
            lstSucursales = new List<DTO_Sucursal>();
        }
    }
}
