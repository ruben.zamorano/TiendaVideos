﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseMVC.Helpers.DTO.Sucursal
{
    public class DTO_ReporteMembresia
    {
        public int nIdTipoMembresia { get; set; }
        public string cTipoMembresia { get; set; }
        public decimal nTotalRentas { get; set; }
        public int nVideosRentados { get; set; }
        public string cFechaRegistro { get; set; }
    }
}