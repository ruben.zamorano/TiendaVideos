﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseMVC.Helpers.DTO.Sucursal
{
    public class DTO_ReporteCliente
    {
        public int nIdCliente { get; set; }
        public string cNombreCliente { get; set; }
        public string cClaveCliene { get; set; }
        public decimal nTotalRentas { get; set; }
        public int nVideosRentados { get; set; }
        public string cFechaRegistro { get; set; }
    }
}