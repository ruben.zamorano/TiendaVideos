﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseMVC.Helpers.DTO.Sucursal
{
    public class DTO_Sucursal
    {
        public int nIdSucursal { get; set; }
        public string cDescripcion { get; set; }
    }
}