using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseMVC.Helpers.DTO.Sucursal
{
    public class DTO_CargaInicialClientes
    {
        public string cUrl { get; set; }
        public bool bError { get; set; }
        public string cMensaje { get; set; }

        public DTO_CargaInicialClientes()
        {
            bError = false;
        }
    }
}
