using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseMVC.Helpers.DTO.Sucursal
{
    public class DTO_CargaInicialReportes
    {
        public string cUrl { get; set; }
        public bool bError { get; set; }
        public string cMensaje { get; set; }
        public List<DTO_ReporteCliente> lstClientes { get; set; }
        public List<DTO_ReporteMembresia> lstGold { get; set; }
        public List<DTO_ReporteMembresia> lstNormal { get; set; }
        public List<DTO_ReporteCategoria> lstInfantil { get; set; }
        public List<DTO_ReporteCategoria> lstAdolescentes { get; set; }
        public List<DTO_ReporteCategoria> lstAdultos { get; set; }

        public DTO_CargaInicialReportes()
        {
            bError = false;
            lstClientes = new List<DTO_ReporteCliente>();
            lstGold = new List<DTO_ReporteMembresia>();
            lstNormal = new List<DTO_ReporteMembresia>();
            lstInfantil = new List<DTO_ReporteCategoria>();
            lstAdolescentes = new List<DTO_ReporteCategoria>();
            lstAdultos = new List<DTO_ReporteCategoria>();
        }
    }
}
