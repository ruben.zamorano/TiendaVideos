﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseMVC.Helpers.DTO.Admin
{
    public class DTO_AreaMVC
    {
        public int nIdArea { get; set; }
        public string cNombreArea { get; set; }
        public string cComentario { get; set; }
        public List<DTO_ModuloMVC> lstModulos { get; set; }
        public List<DTO_Perfil> lstPerfiles { get; set; }

        public DTO_AreaMVC()
        {
            lstModulos = new List<DTO_ModuloMVC>();
            lstPerfiles = new List<DTO_Perfil>();
        }
    }
}