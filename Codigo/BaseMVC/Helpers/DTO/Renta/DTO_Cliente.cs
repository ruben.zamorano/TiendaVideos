﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseMVC.Helpers.DTO.Renta
{
    public class DTO_Cliente
    {
        public int nIdCliente { get; set; }
        public string cClaveCliente { get; set; }
        public int nIdTipoMembresia { get; set; }
        public string cTipoMembresia { get; set; }
        public string cNombreCliente { get; set; }
    }
}