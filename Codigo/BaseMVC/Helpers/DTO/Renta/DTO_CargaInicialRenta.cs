using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseMVC.Helpers.DTO.Renta
{
    public class DTO_CargaInicialRenta
    {
        public string cUrl { get; set; }
        public bool bError { get; set; }
        public string cMensaje { get; set; }
        public List<DTO_Video> lstVideos { get; set; }
        public List<DTO_Cliente> lstClientes { get; set; }

        public DTO_CargaInicialRenta()
        {
            bError = false;
            lstVideos = new List<DTO_Video>();
            lstClientes = new List<DTO_Cliente>();

        }
    }
}
