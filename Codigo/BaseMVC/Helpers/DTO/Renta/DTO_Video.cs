﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseMVC.Helpers.DTO.Renta
{
    public class DTO_Video
    {
        public int nIdVideo { get; set; }
        public string cDescripcion { get; set; }
        public decimal nPrecio { get; set; }
        public int nCopias { get; set; }
        public int nIdCategoria { get; set; }
        public string cCategoria { get; set; }
    }
}