using BaseMVC.Areas.Sucursal.Models;
using BaseMVC.Helpers;
using BaseMVC.Helpers.DTO.Sucursal;
using BaseMVC.Helpers.Funciones;
using BaseMVC.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BaseMVC.Areas.Sucursal.Controllers
{
    public class SucursalesController : Controller
    {
        BaseMVCDataContext db = new BaseMVCDataContext();
        JavaScriptSerializer js = new JavaScriptSerializer();

        public ActionResult Index()
        {
            SucursalesModel model = new SucursalesModel();
            model.cCargaInicial = model.CargaInicial(js,db);
            return View("Index",model);
        }

        [HttpPost]
        [AllowAnonymous]//Este encabezado es para que el metodo pueda ser llamado desde la vista por un ajax
        [BaseMVCAntiForgeryToken]//Este encabezado valida el token del ajax
        public ActionResult GuardarSucursal(DTO_Sucursal Sucursal)
        {
            bool bError = false;
            string cMensaje = "holis";
            try
            {
                db.Connection.Open();
                db.Transaction = db.Connection.BeginTransaction();

                CTL_Sucursales InsertSucursal = db.CTL_Sucursales.Where(x => x.nIdSucursal == Sucursal.nIdSucursal).SingleOrDefault() ?? new CTL_Sucursales();

                if (InsertSucursal.nIdSucursal != 0)
                {
                    InsertSucursal.cDescripcion = Sucursal.cDescripcion;
                    InsertSucursal.dFechaUltimaModificacion = DateTime.Now;
                    InsertSucursal.cUsuarioRegistro = "Admin";
                    InsertSucursal.cMaquinaUltimaModificacion = Environment.MachineName;
                    db.SubmitChanges();
                }
                else
                {
                    db.CTL_Sucursales.InsertOnSubmit(new CTL_Sucursales { 
                        cDescripcion = Sucursal.cDescripcion,
                        dFechaRegistro = DateTime.Now,
                        cUsuarioRegistro = "Admin",
                        cMaquinaRegistro = Environment.MachineName
                    });
                    db.SubmitChanges();
                }

                db.Transaction.Commit();

            }
            catch (Exception e)
            {
                db.Transaction.Rollback();
                bError = true;
            }

            finally
            {
                db.Transaction.Dispose();
            }
            return Json(new
            {
                cMensaje,
                bError,
                Sucursal
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]//Este encabezado es para que el metodo pueda ser llamado desde la vista por un ajax
        [BaseMVCAntiForgeryToken]//Este encabezado valida el token del ajax
        public ActionResult MetodoEjemplo(string someValue)
        {
            bool bError = false;
            string cMensaje = "holis";
            try
            {
                db.Connection.Open();
                db.Transaction = db.Connection.BeginTransaction();

                db.Transaction.Commit();

            }
            catch (Exception e)
            {
                db.Transaction.Rollback();
                bError = true;
            }

            finally
            {
                db.Transaction.Dispose();
            }
            return Json(new
            {
                cMensaje,
                bError
            }, JsonRequestBehavior.AllowGet);
        }
       


       


        [AllowAnonymous]//Este encabezado es para que el metodo pueda ser llamado desde la vista por un ajax
        //[BaseMVCAntiForgeryToken]//Este encabezado valida el token del ajax
        public ActionResult InsertVideos()
        {
            bool bError = false;
            string cMensaje = "holis";
            try
            {
                db.Connection.Open();
                db.Transaction = db.Connection.BeginTransaction();

                Random r = new Random();
                foreach (var v in db.CTL_VIdeos)
                {
                    db.CTL_VideosSucursales.InsertOnSubmit(new CTL_VideosSucursales { 
                        nIdSucursal = 1,
                        nIdVideo = v.nIdVideo,
                        nPrecio = r.Next(200),
                        nCopiasDisponibles = r.Next(100),
                        dFechaRegistro = DateTime.Now,
                        cUsuarioRegistro = "admin",
                        cMaquinaRegistro = Environment.MachineName
                    });
                    db.SubmitChanges();
                
                }

                db.Transaction.Commit();

            }
            catch (Exception e)
            {
                db.Transaction.Rollback();
                bError = true;
            }

            finally
            {
                db.Transaction.Dispose();
            }
            return Json(new
            {
                cMensaje,
                bError
            }, JsonRequestBehavior.AllowGet);
        }


        [AllowAnonymous]//Este encabezado es para que el metodo pueda ser llamado desde la vista por un ajax
        //[BaseMVCAntiForgeryToken]//Este encabezado valida el token del ajax
        public ActionResult InsertUsuarios()
        {
            bool bError = false;
            string cMensaje = "holis";
            try
            {
                db.Connection.Open();
                db.Transaction = db.Connection.BeginTransaction();

                Random r = new Random();

               

                for (int i = 0; i <= 10; i++ )
                {

                    string FolioCliente = db.CTL_Clientes.Select(x => x.Codigo_Cliente).ToList().LastOrDefault();
                    db.CTL_Clientes.InsertOnSubmit(new CTL_Clientes {
                        Codigo_Cliente = FolioCliente == null ? "USR-0000" : Encriptado.SumaFolio(FolioCliente,1),
                        cNombreCliente = "Cliente-" + i,
                        nIdTipoMembresia = r.Next(1,2),
                        dFechaRegistro = DateTime.Now,
                        cUsuarioRegistro = "admin",
                        cMaquinaRegistro = Environment.MachineName
                    });
                    db.SubmitChanges();
                }
                

                db.Transaction.Commit();

            }
            catch (Exception e)
            {
                db.Transaction.Rollback();
                bError = true;
            }

            finally
            {
                db.Transaction.Dispose();
            }
            return Json(new
            {
                cMensaje,
                bError
            }, JsonRequestBehavior.AllowGet);
        }

    }
}
