using BaseMVC.Areas.Sucursal.Models;
using BaseMVC.Helpers.Funciones;
using BaseMVC.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BaseMVC.Areas.Sucursal.Controllers
{
    public class ReportesController : Controller
    {
        BaseMVCDataContext db = new BaseMVCDataContext();
        JavaScriptSerializer js = new JavaScriptSerializer();

        public ActionResult Index()
        {
            ReportesModel model = new ReportesModel();
            model.cCargaInicial = model.CargaInicial(js,db);
            return View("Index",model);
        }
       


        [HttpPost]
        [AllowAnonymous]//Este encabezado es para que el metodo pueda ser llamado desde la vista por un ajax
        [BaseMVCAntiForgeryToken]//Este encabezado valida el token del ajax
        public ActionResult MetodoEjemplo(string someValue)
        {
            bool bError = false;
            string cMensaje = "holis";
            try
            {
                db.Connection.Open();
                db.Transaction = db.Connection.BeginTransaction();

                db.Transaction.Commit();

            }
            catch (Exception e)
            {
                db.Transaction.Rollback();
                bError = true;
            }

            finally
            {
                db.Transaction.Dispose();
            }
            return Json(new {
                cMensaje,
                bError
            }, JsonRequestBehavior.AllowGet);
        }

    }
}
