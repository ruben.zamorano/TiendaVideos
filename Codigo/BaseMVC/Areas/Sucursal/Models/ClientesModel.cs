using BaseMVC.Helpers.DTO.Sucursal;
using BaseMVC.Linq;
using BaseMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Script.Serialization;

namespace BaseMVC.Areas.Sucursal.Models
{
    public class ClientesModel: CargaInicial
    {
        public string cCargaInicial { get; set; }
        public ClientesModel()
        {
            cModuloActual = "Clientes";
            nIdAreaActual = 12;
        }
        public string CargaInicial(JavaScriptSerializer js)
        {

            DTO_CargaInicialClientes CargaInicial = new DTO_CargaInicialClientes();
            string cCargaInicial = "";
            try
            {
                CargaInicial.cUrl = "/Sucursal/Clientes/";
                CargaInicial.cUrl = Convert.ToBase64String(Encoding.UTF8.GetBytes(CargaInicial.cUrl));
            }
            catch (Exception e)
            {
                CargaInicial.bError = true;
                CargaInicial.cMensaje = e.Message;
            }

            cCargaInicial = js.Serialize(CargaInicial);
            return cCargaInicial;
        }

    }
}
