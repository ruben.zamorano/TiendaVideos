using BaseMVC.Helpers.DTO.Sucursal;
using BaseMVC.Linq;
using BaseMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Script.Serialization;

namespace BaseMVC.Areas.Sucursal.Models
{
    public class SucursalesModel: CargaInicial
    {
        public string cCargaInicial { get; set; }
        public SucursalesModel()
        {
            cModuloActual = "Sucursales";
            nIdAreaActual = 12;
        }
        public string CargaInicial(JavaScriptSerializer js, BaseMVCDataContext db)
        {

            DTO_CargaInicialSucursales CargaInicial = new DTO_CargaInicialSucursales();
            string cCargaInicial = "";
            try
            {
                CargaInicial.cUrl = "/Sucursal/Sucursales/";
                CargaInicial.cUrl = Convert.ToBase64String(Encoding.UTF8.GetBytes(CargaInicial.cUrl));
                CargaInicial.lstSucursales = db.CTL_Sucursales.Select(s => new DTO_Sucursal { 
                    nIdSucursal = s.nIdSucursal,
                    cDescripcion = s.cDescripcion
                }).ToList();
            }
            catch (Exception e)
            {
                CargaInicial.bError = true;
                CargaInicial.cMensaje = e.Message;
            }

            cCargaInicial = js.Serialize(CargaInicial);
            return cCargaInicial;
        }

    }
}
