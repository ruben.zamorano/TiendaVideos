using BaseMVC.Helpers.DTO.Sucursal;
using BaseMVC.Linq;
using BaseMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Script.Serialization;

namespace BaseMVC.Areas.Sucursal.Models
{
    public class ReportesModel: CargaInicial
    {
        public string cCargaInicial { get; set; }
        public List<DTO_ReporteMembresia> lstReportesMembresias { get; set; }
        public List<DTO_ReporteCategoria> lstReportesCategorias { get; set; }
        public ReportesModel()
        {
            cModuloActual = "Reportes";
            nIdAreaActual = 12;
            lstReportesCategorias = new List<DTO_ReporteCategoria>();
            lstReportesMembresias = new List<DTO_ReporteMembresia>();
        }
        public string CargaInicial(JavaScriptSerializer js, BaseMVCDataContext db)
        {

            DTO_CargaInicialReportes CargaInicial = new DTO_CargaInicialReportes();
            string cCargaInicial = "";
            try
            {
                CargaInicial.cUrl = "/Sucursal/Reportes/";
                CargaInicial.cUrl = Convert.ToBase64String(Encoding.UTF8.GetBytes(CargaInicial.cUrl));
                CargaInicial.lstClientes = ObtenerlstClientes(db);
                lstReportesCategorias = ObtenerReporteClasificacion(db, DateTime.Now);
                lstReportesMembresias = ObtenerReporteMembresias(db, DateTime.Now, DateTime.Now);
                

            }
            catch (Exception e)
            {
                CargaInicial.bError = true;
                CargaInicial.cMensaje = e.Message;
            }

            cCargaInicial = js.Serialize(CargaInicial);
            return cCargaInicial;
        }

            //lstClientes = new List<DTO_ReporteCliente>();
            //lstGold = new List<DTO_ReporteMembresia>();
            //lstNormal = new List<DTO_ReporteMembresia>();
            //lstInfantil = new List<DTO_ReporteCategoria>();
            //lstAdolescentes = new List<DTO_ReporteCategoria>();
            //lstAdultos = new List<DTO_ReporteCategoria>();

        public List<DTO_ReporteCliente> ObtenerlstClientes(BaseMVCDataContext db)
        {
            List<DTO_ReporteCliente> lstClientes = (from c in db.CTL_Clientes
                                                    join rt in db.RT_Videos
                                                    on c.nIdCliente equals rt.nIdCliente
                                                    into rts 
                                                    from rt in rts.DefaultIfEmpty()
                                                    select new DTO_ReporteCliente { 
                                                        nIdCliente = c.nIdCliente,
                                                        cNombreCliente = c.cNombreCliente,
                                                        cClaveCliene = c.Codigo_Cliente ,
                                                        nTotalRentas = rt == null ? 0 : rts.Sum(x => x.nImporteRenta),
                                                        nVideosRentados = rt == null ? 0 : rts.Sum(x => x.nCopiasRentadas)

                                                    }).Distinct().ToList();
                
             

            return lstClientes;
        }

        public List<DTO_ReporteMembresia> ObtenerReporteMembresias(BaseMVCDataContext db, DateTime dFechaInicio, DateTime dFechaFin)
        {
            List<DTO_ReporteMembresia> lstMembresias =

                (from m in db.CTL_TiposMembresias
                 join c in db.CTL_Clientes
                 on m.nIdTipoMembresia equals c.nIdTipoMembresia
                 join rt in db.RT_Videos
                 on c.nIdCliente equals rt.nIdCliente
                 into rts
                 from rt in rts.DefaultIfEmpty()
                 select new DTO_ReporteMembresia
                 {
                     nIdTipoMembresia = m.nIdTipoMembresia,
                     cTipoMembresia = m.cDescripcion,
                     nTotalRentas = rt == null ? 0 : rts.Sum(x => x.nImporteRenta),
                     nVideosRentados = rt == null ? 0 : rts.Sum(x => x.nCopiasRentadas)
                 }).Distinct().ToList();

            return lstMembresias;
        }

        public List<DTO_ReporteCategoria> ObtenerReporteClasificacion(BaseMVCDataContext db, DateTime dFechaInicio)
        {
            List<DTO_ReporteCategoria> lstCategorias =

            //    db.CTL_Categorias.Select(c => new DTO_ReporteCategoria {
                //    nIdCategoria = c.nIdCategoria,
                //    cTipoCategoria = c.cDescripcion,
                //    nVideosRentados = c.CTL_VIdeos == null ? 0 : 
                //                      c.CTL_VIdeos.Sum(x => 
                //                          x.CTL_VideosSucursales == null ? 0 :  
                //                          x.CTL_VideosSucursales.Sum(y => 
                //                              y.RT_VideosDetalle == null ? 0 :
                //                              y.RT_VideosDetalle.Sum(z => z.RT_Videos.nCopiasRentadas))),



            //    nTotalRentas = c.CTL_VIdeos == null ? 0 :  
                //                   c.CTL_VIdeos.Sum(x => 
                //                       x.CTL_VideosSucursales == null ? 0 :  
                //                       x.CTL_VideosSucursales.Sum(y => 
                //                           y.RT_VideosDetalle == null ? 0 : 
                //                           y.RT_VideosDetalle.Sum(z => z.RT_Videos.nImporteRenta)))
                //}).ToList() ?? new List<DTO_ReporteCategoria>();

                (from c in db.CTL_Categorias
                 join v in db.CTL_VIdeos
                 on c.nIdCategoria equals v.nIdCategoria
                 join rt in db.RT_VideosDetalle
                 on v.nIdVideo equals rt.CTL_VideosSucursales.CTL_VIdeos.nIdVideo
                 into rts
                 from rt in rts.DefaultIfEmpty()
                 select new DTO_ReporteCategoria
                 {
                     nIdCategoria = c.nIdCategoria,
                     cTipoCategoria = c.cDescripcion,
                     nTotalRentas = rt == null ? 0 : rts.Sum(x => x.CTL_VideosSucursales.nPrecio),
                     nVideosRentados = rt == null ? 0 : rts.Count()
                 }).DistinctBy(r => r.nIdCategoria).ToList();

            return lstCategorias;
        }

        public static IEnumerable<TSourse> DistinctBy<TSourse, TKey>(this IEnumerable<TSourse> sourse, Func<TSourse, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach(TSourse element in sourse)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }

    }
}
