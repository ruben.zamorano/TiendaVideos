using BaseMVC.Helpers.DTO.Renta;
using BaseMVC.Linq;
using BaseMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Script.Serialization;

namespace BaseMVC.Areas.Renta.Models
{
    public class RentaModel: CargaInicial
    {
        public string cCargaInicial { get; set; }
        public RentaModel()
        {
            cModuloActual = "Renta";
            nIdAreaActual = 11;
        }
        public string CargaInicial(JavaScriptSerializer js, BaseMVCDataContext db)
        {

            DTO_CargaInicialRenta CargaInicial = new DTO_CargaInicialRenta();
            string cCargaInicial = "";
            try
            {
                CargaInicial.cUrl = "/Renta/Renta/";
                CargaInicial.cUrl = Convert.ToBase64String(Encoding.UTF8.GetBytes(CargaInicial.cUrl));
                CargaInicial.lstVideos = db.CTL_VideosSucursales.Where(x => x.nIdSucursal == 1 && x.nCopiasDisponibles > 0).Select(v => new DTO_Video { 
                    nIdVideo = v.nIdVideo,
                    cDescripcion = v.CTL_VIdeos.cDescripcion,
                    nCopias = v.nCopiasDisponibles,
                    nPrecio = v.nPrecio,
                    nIdCategoria = v.CTL_VIdeos.nIdCategoria ,
                    cCategoria = v.CTL_VIdeos.CTL_Categorias.cDescripcion
                }).ToList();

                CargaInicial.lstClientes = db.CTL_Clientes.Select(x => new DTO_Cliente { 
                    nIdCliente = x.nIdCliente,
                    cNombreCliente = x.cNombreCliente,
                    cClaveCliente = x.Codigo_Cliente,
                    nIdTipoMembresia = x.nIdTipoMembresia,
                    cTipoMembresia = x.CTL_TiposMembresias.cDescripcion
                }).ToList();
            }
            catch (Exception e)
            {
                CargaInicial.bError = true;
                CargaInicial.cMensaje = e.Message;
            }

            cCargaInicial = js.Serialize(CargaInicial);
            return cCargaInicial;
        }

    }
}
