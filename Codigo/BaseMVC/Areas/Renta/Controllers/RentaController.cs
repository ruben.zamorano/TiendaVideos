using BaseMVC.Areas.Renta.Models;
using BaseMVC.Helpers.DTO.Renta;
using BaseMVC.Helpers.Funciones;
using BaseMVC.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BaseMVC.Areas.Renta.Controllers
{
    public class RentaController : Controller
    {
        BaseMVCDataContext db = new BaseMVCDataContext();
        JavaScriptSerializer js = new JavaScriptSerializer();

        public ActionResult Index()
        {
            RentaModel model = new RentaModel();
            model.cCargaInicial = model.CargaInicial(js,db);
            return View("Index",model);
        }


        [HttpPost]
        [AllowAnonymous]//Este encabezado es para que el metodo pueda ser llamado desde la vista por un ajax
        [BaseMVCAntiForgeryToken]//Este encabezado valida el token del ajax
        public ActionResult GuardarRenta(int nIdCliente, string clstVideos)
        {
            bool bError = false;
            string cMensaje = "Venta guardada correctamente";
            List<DTO_Video> lstVideos = js.Deserialize<List<DTO_Video>>(clstVideos);
            try
            {
                db.Connection.Open();
                db.Transaction = db.Connection.BeginTransaction();

                RT_Videos InsertVideo = new RT_Videos
                {
                    nIdCliente = nIdCliente,
                    nCopiasRentadas = lstVideos.Sum(x => x.nCopias),
                    nImporteRenta = lstVideos.Sum(x => x.nPrecio),
                    dFechaRegistro = DateTime.Now,
                    cUsuarioRegistro = "Admin",
                    cMaquinaRegistro = Environment.MachineName,
                    bActivo = true
                };

                db.RT_Videos.InsertOnSubmit(InsertVideo);
                db.SubmitChanges();

                db.RT_VideosDetalle.InsertAllOnSubmit(lstVideos.Select(x => new RT_VideosDetalle{
                    nIdRentaVideos = InsertVideo.nIdRentaVideos,
                    nIdVideoSucursal = x.nIdVideo,
                    nCopias = x.nCopias,
                    bActivo = true,
                    dFechaRegistro = DateTime.Now,
                    cUsuarioRegistro = "Admin",
                    cMaquinaRegistro = Environment.MachineName
                }).ToList());
                db.SubmitChanges();

                foreach (var v in lstVideos)
                {
                    CTL_VideosSucursales Video = db.CTL_VideosSucursales.Where(x => x.nIdVideo == v.nIdVideo).SingleOrDefault();
                    Video.nCopiasDisponibles = Video.nCopiasDisponibles - v.nCopias;
                    db.SubmitChanges();
                }

                lstVideos = db.CTL_VideosSucursales.Where(x => x.nIdSucursal == 1 && x.nCopiasDisponibles > 0).Select(v => new DTO_Video
                {
                    nIdVideo = v.nIdVideo,
                    cDescripcion = v.CTL_VIdeos.cDescripcion,
                    nCopias = v.nCopiasDisponibles,
                    nPrecio = v.nPrecio,
                    nIdCategoria = v.CTL_VIdeos.nIdCategoria,
                    cCategoria = v.CTL_VIdeos.CTL_Categorias.cDescripcion
                }).ToList();

                
                db.Transaction.Commit();

            }
            catch (Exception e)
            {
                db.Transaction.Rollback();
                bError = true;
            }

            finally
            {
                db.Transaction.Dispose();
            }
            return Json(new
            {
                cMensaje,
                bError ,
                lstVideos
            }, JsonRequestBehavior.AllowGet);
        }
       


        [HttpPost]
        [AllowAnonymous]//Este encabezado es para que el metodo pueda ser llamado desde la vista por un ajax
        [BaseMVCAntiForgeryToken]//Este encabezado valida el token del ajax
        public ActionResult MetodoEjemplo(string someValue)
        {
            bool bError = false;
            string cMensaje = "holis";
            try
            {
                db.Connection.Open();
                db.Transaction = db.Connection.BeginTransaction();

                db.Transaction.Commit();

            }
            catch (Exception e)
            {
                db.Transaction.Rollback();
                bError = true;
            }

            finally
            {
                db.Transaction.Dispose();
            }
            return Json(new {
                cMensaje,
                bError
            }, JsonRequestBehavior.AllowGet);
        }

    }
}
