﻿
var BaseMVC = angular.module('BaseMVC', ['ngMaterial', 'md.data.table']);



BaseMVC.config(['$mdThemingProvider', function ($mdThemingProvider) {
    var customPrimary = {
        '50': '#99c5de',
        '100': '#86bad8',
        '200': '#72afd2',
        '300': '#5fa4cc',
        '400': '#4c99c6',
        '500': '#3C8DBC',
        '600': '#367ea9',
        '700': '#307095',
        '800': '#296182',
        '900': '#23536f',
        'A100': '#acd0e5',
        'A200': '#c0dbeb',
        'A400': '#d3e6f1',
        'A700': '#1d445b'
    };
    $mdThemingProvider
        .definePalette('customPrimary',
                        customPrimary);

    var customAccent = {
        '50': '#000000',
        '100': '#000000',
        '200': '#040607',
        '300': '#0d1418',
        '400': '#162328',
        '500': '#1f3139',
        '600': '#314d59',
        '700': '#3a5b6a',
        '800': '#436a7a',
        '900': '#4c788b',
        'A100': '#314d59',
        'A200': '#283F49',
        'A400': '#1f3139',
        'A700': '#55869b'
    };
    $mdThemingProvider.definePalette('customAccent', customAccent);



    var customBackground = {
        '50': '#737373',
        '100': '#666666',
        '200': '#595959',
        '300': '#4d4d4d',
        '400': '#404040',
        '500': '#333',
        '600': '#262626',
        '700': '#1a1a1a',
        '800': '#0d0d0d',
        '900': '#000000',
        'A100': '#808080',
        'A200': '#8c8c8c',
        'A400': '#999999',
        'A700': '#000000'
    };
    $mdThemingProvider.definePalette('customBackground', customBackground);



    $mdThemingProvider.theme('default')
    .primaryPalette('customPrimary')
    .accentPalette('customAccent');
    //.warnPalette('customWarn')
    //.backgroundPalette('customBackground');
}]);



//BaseMVC.factory(['Fun', function () {

//    this.MensageGenerico = function ($mdDialog, Encabezado, Mensaje) {
//        $mdDialog.show(
//              $mdDialog.alert()
//                .parent(angular.element(document.querySelector('#Body_Layout')))
//                .clickOutsideToClose(true)
//                .title(Encabezado)
//                .textContent(Texto)
//                .ariaLabel('Alert Dialog Demo')
//                .ok('Entendido')
//            );
//    }

//}]);
BaseMVC.factory("Fun", function () {
    return {
        MensajeGenerico: function ($mdDialog, Encabezado, Mensaje) {
                    $mdDialog.show(
                          $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#Body_Layout')))
                            .clickOutsideToClose(true)
                            .title(Encabezado)
                            .textContent(Mensaje)
                            .ariaLabel('Alert Dialog Demo')
                            .ok('Entendido')
                        );
        },
        Base64: function (encoded) {
            var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
            var output = "";
            var chr1, chr2, chr3;
            var enc1, enc2, enc3, enc4;
            var i = 0;

            do {
                enc1 = keyStr.indexOf(encoded.charAt(i++));
                enc2 = keyStr.indexOf(encoded.charAt(i++));
                enc3 = keyStr.indexOf(encoded.charAt(i++));
                enc4 = keyStr.indexOf(encoded.charAt(i++));

                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;

                output = output + String.fromCharCode(chr1);

                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }
            } while (i < encoded.length);

            return output;
        },

        alerta: function (url) {
            alert(this.Base64(url));
        },

        http2: function ($http, url, Metodo, Token, data) {
            var Response = {};
            $http({
                headers: {
                    'RequestVerificationToken': Token
                },
                url: this.Base64(url) + Metodo,
                method: "POST",
                params: data,
            }).success(function (response) {
                debugger
                Response = response;
            }).error(function (error) {
                alert("asd");
            });

            return Response;
        }
    };
})

