
BaseMVC.controller('RentaCtrl', ['$http', '$scope', '$filter', '$q', '$timeout', '$log', '$mdDialog', '$mdMedia','Fun', function ($http, $scope, $filter, $q, $timeout, $log, $mdDialog, $mdMedia,Fun) {

    $scope.cUrlModulo = "Renta";
    $scope.CargaInicial = {};
    $scope.ClientesAtc = {
        searchText: ""
    };
    $scope.VideosAtc = {
        searchText: ""
    };
    $scope.bPantallaCliente = true;

    $scope.Cliente2 = {};
    $scope.lstVideos = [];


    $scope.CargaInicial = function (cCargaInicial,Token) {
        debugger
        $scope.CargaInicial = cCargaInicial;
        $scope.antiForgeryToken = Token;
        $scope.CargaInicial.cUrl = Fun.Base64($scope.CargaInicial.cUrl);

        console.log($scope.CargaInicial.lstVideos);
    }

    $scope.PantallaVideo = function (item)
    {
        $scope.Cliente2 = JSON.parse(JSON.stringify(item));
        $scope.bPantallaCliente = false;
    }

    $scope.AgregarVideo = function (item)
    {
        var video = JSON.parse(JSON.stringify(item));
        video.nCopias = 1;
        $scope.lstVideos.push(video);
    }


    $scope.GuardarRenta = function ()
    {
        $http({
            headers: {
                'RequestVerificationToken': $scope.antiForgeryToken
            },
            url: $scope.CargaInicial.cUrl + "GuardarRenta",
            method: "POST",
            params: {
                'nIdCliente': $scope.Cliente2.nIdCliente,
                'clstVideos': JSON.stringify($scope.lstVideos)
            },
        }).success(function (response) {
            debugger
            Fun.MensajeGenerico($mdDialog, "Exito!", response.cMensaje);
            $scope.CargaInicial.lstVideos = response.lstVideos;
            $scope.bPantallaCliente = true;
            $scope.Cliente2 = {};
            $scope.lstVideos = [];

            

        }).error(function (error) {
            Fun.MensajeGenerico($mdDialog, "Alerta!", "No estas autorizado para esta realizar esta accion");
        });

    }

    

    $scope.prueba = function () {
        debugger    
        $http({
            headers: {
                'RequestVerificationToken':  $scope.antiForgeryToken
            } ,
            url: $scope.CargaInicial.cUrl + "MetodoEjemplo",
            method: "POST",
            params: {
                'someValue': "asdas"
            },
        }).success(function (response) {
            debugger
            Fun.MensajeGenerico($mdDialog, "Exito!", response.cMensaje);

        }).error(function (error) {
            Fun.MensajeGenerico($mdDialog, "Alerta!", "No estas autorizado para esta realizar esta accion");
        });
    }

    $scope.prueba2 = function () {
        debugger
        $http({
            headers: {
                'RequestVerificationToken': $scope.antiForgeryToken + "asdas"
            },
            url: $scope.CargaInicial.cUrl + "MetodoEjemplo",
            method: "POST",
            params: {
                'someValue': "asdas"
            },
        }).success(function (response) {
            debugger
            Fun.MensajeGenerico($mdDialog, "Exito!", response.cMensaje);

        }).error(function (error) {
            Fun.MensajeGenerico($mdDialog, "Alerta!", "No estas autorizado para esta realizar esta accion");
        });
    }

   



   





}]);

