﻿
var AdminLTE = angular.module('AdminLTE', ['ngMaterial']);


AdminLTE.config(['$mdThemingProvider', function ($mdThemingProvider) {
          $mdThemingProvider.theme('default')
          .primaryPalette('purple')
          .accentPalette('green', {
              'default': '500'
          });
}]);

