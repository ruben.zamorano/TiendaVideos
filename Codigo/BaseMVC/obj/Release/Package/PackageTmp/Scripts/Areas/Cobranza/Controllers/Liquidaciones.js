﻿
AdminLTE.controller('LiquidacionesCtrl', ['$http', '$scope', '$filter', '$q', '$timeout', '$log', '$mdDialog', '$mdMedia', function ($http, $scope, $filter, $q, $timeout, $log, $mdDialog, $mdMedia) {

    $scope.sizes = [
          "small (12-inch)",
          "medium (14-inch)",
          "large (16-inch)",
          "insane (42-inch)"
    ];
    $scope.toppings = [
      { category: 'meat', name: 'Enero' },
      { category: 'meat', name: 'Febrero' },
      { category: 'meat', name: 'Marzo' },
      { category: 'meat', name: 'Abril' }
    ];
    $scope.selectedToppings = [];
    $scope.printSelectedToppings = function printSelectedToppings() {
        var numberOfToppings = this.selectedToppings.length;
        
        if (numberOfToppings > 1) {
            var needsOxfordComma = numberOfToppings > 2;
            var lastToppingConjunction = (needsOxfordComma ? ',' : '') + ' and ';
            var lastTopping = lastToppingConjunction +
                this.selectedToppings[this.selectedToppings.length - 1];
            return this.selectedToppings.slice(0, -1).join(', ') + lastTopping;
        }
        return this.selectedToppings.join('');
    };


}]);

