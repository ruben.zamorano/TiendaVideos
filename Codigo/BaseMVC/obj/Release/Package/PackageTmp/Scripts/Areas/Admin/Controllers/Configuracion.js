﻿
AdminLTE.controller('InventariosCtrl', ['$http', '$scope', '$filter', '$q', '$timeout', '$log', '$mdDialog', '$mdMedia', function ($http, $scope, $filter, $q, $timeout, $log, $mdDialog, $mdMedia) {

    $scope.CargaInicial = {};
    $scope.CargaInicial = function (cCargaInicial) {
        debugger
        $scope.CargaInicial = cCargaInicial;
        $scope.CargaInicial.bExportar = false;
        $scope.CargaInicial.BusquedaRealizada = false;
        $scope.CentroLogisticoAtc.states = $scope.CargaInicial.lstCentrosLogisticos;
        $scope.AutocompleteMateriales.lstMateriales = $scope.CargaInicial.lstMateriales;
    }


    $scope.OnChangeGrupo = function (item) {
        debugger
        if (item == undefined) {
            $scope.AutocompleteMateriales.lstMateriales = $scope.CargaInicial.lstMateriales;
        }
        else {
            $scope.AutocompleteMateriales.lstMateriales = $filter('filter')($scope.CargaInicial.lstMateriales, { nIdGrupo: item.nIdGrupo });
        }
    }


    $scope.lstResultados = [];


    $scope.BuscarSalidas = function () {
        debugger
        $http({
            url: '/Inventarios/InventarioAdmin/BuscarSalidas',
            method: "POST",
            params: {
                'cFiltrosBusqueda': JSON.stringify($scope.FiltrosBusqueda)
            },
        }).success(function (response) {
            debugger
            $scope.CargaInicial.BusquedaRealizada = true;
            if (!response.bError) {
                $scope.lstResultados = response.lstSalidas;
                if (response.lstSalidas.length > 0) {
                    $scope.CargaInicial.bExportar = true;
                }
                else {
                    $scope.lstResultados = [];
                }

            }

        }).error(function (error) {
            swal('¡Error!', 'Algo salió mal', 'error');
        });
    }

    $scope.ValidarFiltros = function () {
        var bError = false;
        var cMensaje = "";
        if ($scope.FiltrosBusqueda.dFechaDesde > $scope.FiltrosBusqueda.dFechaHasta) {
            bError = true;
            cMensaje = "La Fecha de inicio debe ser menor a la fecha fin"
        }
        if ($scope.FiltrosBusqueda.dFechaDesde == undefined || $scope.FiltrosBusqueda.dFechaHasta == undefined) {
            bError = true;
            cMensaje = "Falta capturar las fechas";
        }


        if (bError) {
            $scope.MensajeGenerico("Alerta", cMensaje);
            return;
        }
        else {
            $scope.BuscarSalidas();

        }


        return bError;
    }

    $scope.MensajeGenerico = function (Encabezado, Texto) {
        $mdDialog.show(
              $mdDialog.alert()
                .parent(angular.element(document.querySelector('#Body_Layout')))
                .clickOutsideToClose(true)
                .title(Encabezado)
                .textContent(Texto)
                .ariaLabel('Alert Dialog Demo')
                .ok('Entendido')
            );

    }


    $scope.AutocompleteMateriales = {
        simulateQuery: true,
        isDisabled: false,
       
    };


    $scope.GrupoArticulosAtc = {
        simulateQuery: true,
        isDisabled: false,
       
    };

    $scope.CentroLogisticoAtc = {
        simulateQuery: true,
        isDisabled: false,
        
    };










    $scope.GuardarRuta = function () {
        $http({
            url: '/MisPlazas/Plazas/GuardarRuta',
            method: "POST",
            params: {
                'cRuta': $scope.Ruta
            },
        }).success(function (response) {
            debugger
        }).error(function (error) {
            swal('¡Error!', 'Algo salió mal', 'error');
        });

    }

    $scope.asd = {
    simulateQuery : false,
   isDisabled    : false,
    // list of `state` value/display objects
   states        : loadAll(),
   querySearch   : querySearch,
   selectedItemChange : selectedItemChange,
   searchTextChange   : searchTextChange,
   newState : newState
    };
    
    function newState(state) {
        alert("Sorry! You'll need to create a Constituion for " + state + " first!");
    }
    // ******************************
    // Internal methods
    // ******************************
    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
        var results = query ? $scope.asd.states.filter( createFilterFor(query) ) : $scope.asd.states,
            deferred;
        if ($scope.asd.simulateQuery) {
            deferred = $q.defer();
            $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
            return deferred.promise;
        } else {
            return results;
        }
    }
    function searchTextChange(text) {
        $log.info('Text changed to ' + text);
    }
    function selectedItemChange(item) {
        $log.info('Item changed to ' + JSON.stringify(item));
    }
    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
        var allStates = 'Alabama, Alaska, Arizona, Arkansas, California, Colorado, Connecticut, Delaware,\
              Florida, Georgia, Hawaii, Idaho, Illinois, Indiana, Iowa, Kansas, Kentucky, Louisiana,\
              Maine, Maryland, Massachusetts, Michigan, Minnesota, Mississippi, Missouri, Montana,\
              Nebraska, Nevada, New Hampshire, New Jersey, New Mexico, New York, North Carolina,\
              North Dakota, Ohio, Oklahoma, Oregon, Pennsylvania, Rhode Island, South Carolina,\
              South Dakota, Tennessee, Texas, Utah, Vermont, Virginia, Washington, West Virginia,\
              Wisconsin, Wyoming';
        return allStates.split(/, +/g).map( function (state) {
            return {
                value: state.toLowerCase(),
                display: state
            };
        });
    }
    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(state) {
            return (state.value.indexOf(lowercaseQuery) === 0);
        };
    }



}]);

